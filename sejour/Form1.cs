﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using sejour.DataAccessLayer;
using sejour.TourVisioService;



namespace sejour
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private Service1SoapClient tourvisioClient = new Service1SoapClient();
        private ilws._xmlgateClient xmlgate = new ilws._xmlgateClient();

        private bool bEnabled = false;
        private List<Hotel> hotels = new List<Hotel>();
        private List<CostofferList> costofferlist = new List<CostofferList>();
        private PricesData pd = new PricesData();
        private List<sejour.Item> Partners = new List<Item>();
        private sejour.HotelData hotelData = new HotelData();
        private Guid Token = Guid.Empty;
        private int PartnerId;
        private List<Item> spotypes = new List<Item>();
        private List<Partner> partners = new List<Partner>();
        private string LoginToTourVisioService, PasswordToTourVisioService;

        private void Form1_Load(object sender, EventArgs e)
        {
            ConnectionString.Value =
               "Data Source=192.168.0.5; Initial Catalog=IL2009; User ID=art; Password=Pa$$4096w0rd$";
            this.Text = tourvisioClient.Endpoint.Address.ToString();
            spotypes.Add(new Item() {Id = 0, Title = @"All"});
            spotypes.Add(new Item() { Id = 1, Title = @"Contracts" });
            spotypes.Add(new Item() { Id = 2, Title = @"Spos" });
            cbPriceOfferTypes.DataSource = spotypes;
            cbPriceOfferTypes.DisplayMember = "Title";
            cbPriceOfferTypes.ValueMember = "Id";
            partners = Methods.GetPartners();
            cbPartners.DataSource = partners;
            cbPartners.ValueMember = "Id";
            cbPartners.DisplayMember = "Title";
            cbPartners.SelectedValue = 7003;

            List<Item> markets = Methods.GetMarkets();
            cbMarkets.DataSource = markets;
            cbMarkets.ValueMember = "Id";
            cbMarkets.DisplayMember = "Title";

            cbMarkets.SelectedValue = 132;

            List<Item> hotelRegions = Methods.GetHotelRegions();
            cbRegions.DataSource = hotelRegions;
            cbRegions.ValueMember = "Id";
            cbRegions.DisplayMember = "Title";

            hotels = Methods.GetHotels((int)cbRegions.SelectedValue);
            cbHotels.DataSource = hotels;
            cbHotels.DisplayMember = "Title";
            cbHotels.ValueMember = "Id";
            cbHotels.SelectedValue = 1303;
            cbCostOfferList.DataSource = null;
            cbCostOfferList.Text = "";
            costofferlist = Methods.GetCostofferList(Convert.ToInt32(cbHotels.SelectedValue));
            cbCostOfferList.DataSource = costofferlist;
            cbCostOfferList.ValueMember = "Id";
            cbCostOfferList.DisplayMember = "Title";

            if ((int)cbPartners.SelectedValue == 7003)
            {
                LoginToTourVisioService = "MOUZ";
                PasswordToTourVisioService = @"FIBULATEST";
            }
            else if ((int)cbPartners.SelectedValue == 2469)
            {
                LoginToTourVisioService = "DORIS";
                PasswordToTourVisioService = @"DORIS$5";
            }

                bEnabled = true;
        }

        private void btnSendContract_Click(object sender, EventArgs e)
        {
            //var SendContractRequest = new sc.SendContractRequest();
            //var ContractHeaders = new Header[]
            //{
            //    new Header()
            //    {
            //        SupplierName = "MOUZENIDIS TRAVEL GREECE",
            //        Hotel = "ALPHA",
            //        HotelName = "ALPHA HOTEL",
            //        Operator = "FIBULA ROM",
            //        OperatorName = "FIBULA ROMANIA",
            //        BegDate = new DateTime(2017, 04, 01),
            //        EndDate = new DateTime(2017, 10, 31),
            //        Currency = "EUR",
            //        PriceOpt = 1,
            //        ResBegDate = new DateTime(2016, 11, 01),
            //        ResEndDate = new DateTime(2017, 10, 31),
            //        PriceType = "C",
            //        Remark = "TEST"
            //    }
            //};
            //var ContractRoomPrice = new RoomPrice[]
            //{
            //     new RoomPrice()
            //        {
            //            Accom = "DOUBLE",
            //            AccomName = "DOUBLE ROOM",
            //            Room = "STD",
            //            RoomName = "STANDARD ROOM",
            //            Board = "HB",
            //            BoardName = "HALF BOARD",
            //            BegDate = new DateTime(2017, 04, 01),
            //            EndDate = new DateTime(2017, 04, 30),
            //            AdultPrice = 100,
            //            ExtBedPrice = 50,
            //            BoardPrice = 10,
            //            PriceOpt = "Y",
            //            PriceType = "O",
            //            StdAdl = 2,
            //            MinAdl = 2,
            //            MaxAdl = 3,
            //            MaxChd = 0,
            //            MaxPax = 3
            //        },

            //};
            //var ContractChildPrice = new ChdPrice[]
            //{
            //new ChdPrice()
            //   {
            //       //BegDate = new DateTime(2017, 04, 01),
            //       //EndDate = new DateTime(2017, 04, 30),
            //       //Adl = 2,
            //       //Room = "Double Room",
            //       //Board = "HB",
            //       //C1Age1 = 0,
            //       //C1Age2 = 11,
            //       //C1Per = 1,
            //       //C1Price = 20
            //   },
            //new ChdPrice()
            //{
            //    BegDate = new DateTime(2017, 05, 01),
            //    EndDate = new DateTime(2017, 06, 30),
            //    Adl = 2,
            //    Room = "Double Room",
            //    Board = "HB",
            //    C1Age1 = 0,
            //    C1Age2 = 11,
            //    C1Per = 1,
            //    C1Price = 20
            //},
            //new ChdPrice()
            //{
            //    BegDate = new DateTime(2017, 07, 01),
            //    EndDate = new DateTime(2017, 08, 10),
            //    Adl = 2,
            //    Room = "Double Room",
            //    Board = "HB",
            //    C1Age1 = 0,
            //    C1Age2 = 11,
            //    C1Per = 1,
            //    C1Price = 20
            //},
            //new ChdPrice()
            //{
            //    BegDate = new DateTime(2017, 08, 11),
            //    EndDate = new DateTime(2017, 08, 26),
            //    Adl = 2,
            //    Room = "Double Room",
            //    Board = "HB",
            //    C1Age1 = 0,
            //    C1Age2 = 11,
            //    C1Per = 1,
            //    C1Price = 20
            //},
            //new ChdPrice()
            //{
            //    BegDate = new DateTime(2017, 08, 27),
            //    EndDate = new DateTime(2017, 09, 30),
            //    Adl = 2,
            //    Room = "Double Room",
            //    Board = "HB",
            //    C1Age1 = 0,
            //    C1Age2 = 11,
            //    C1Per = 1,
            //    C1Price = 20
            //},
            //new ChdPrice()
            //{
            //    BegDate = new DateTime(2017, 10, 01),
            //    EndDate = new DateTime(2017, 10, 31),
            //    Adl = 2,
            //    Room = "Double Room",
            //    Board = "HB",
            //    C1Age1 = 0,
            //    C1Age2 = 11,
            //    C1Per = 1,
            //    C1Price = 20
            //}
            //};

            //var ContractOptions = new Option[]
            //{
            //      new Option()
            //        {
            //            Room = "STD",
            //            BegDate = new DateTime(2017, 04, 01),
            //            EndDate = new DateTime(2017, 04, 30),
            //            ReleaseDay = 5
            //        },
            //        //new Option()
            //        //{
            //        //    Room = "Double Room",
            //        //    BegDate = new DateTime(2017, 05, 01),
            //        //    EndDate = new DateTime(2017, 06, 30),
            //        //    ReleaseDay = 5
            //        //},
            //        //new Option()
            //        //{
            //        //    Room = "Double Room",
            //        //    BegDate = new DateTime(2017, 07, 01),
            //        //    EndDate = new DateTime(2017, 08, 10),
            //        //    ReleaseDay = 5
            //        //},
            //        //new Option()
            //        //{
            //        //    Room = "Double Room",
            //        //    BegDate = new DateTime(2017, 08, 11),
            //        //    EndDate = new DateTime(2017, 08, 26),
            //        //    ReleaseDay = 5
            //        //},
            //        //new Option()
            //        //{
            //        //    Room = "Double Room",
            //        //    BegDate = new DateTime(2017, 08, 27),
            //        //    EndDate = new DateTime(2017, 09, 30),
            //        //    ReleaseDay = 5
            //        //},
            //        //new Option()
            //        //{
            //        //    Room = "Double Room",
            //        //    BegDate = new DateTime(2017, 10, 01),
            //        //    EndDate = new DateTime(2017, 10, 31),
            //        //    ReleaseDay = 5
            //        //}
            //};
            //var ContractSpos = new SPO[]
            //{
            //    new SPO()
            //    {
            //        SpoNo = 1,
            //        SpoType = "EB",
            //        SellBegDate = Convert.ToDateTime("2016-11-01"),
            //        SellEndDate = Convert.ToDateTime("2017-10-31"),
            //        Accom = "DBL",
            //        Room = "STD",
            //        Board = "HB",
            //        ResBegDate = Convert.ToDateTime("2017-04-01"),
            //        ResEndDate = Convert.ToDateTime("2017-04-30"),
            //        XStay = 0,
            //        YPay = 0,
            //        Price = 100,
            //        ExtBedPrice = 20,
            //        BoardPrice = 0,
            //        PriceOpt = 1,
            //        Remark = "TEST EB",
            //        PriceType = "O",
            //        DaySpoType = 1,
            //        EBSellPer = 10,
            //        EBSellPrice = 0,
            //        SpoOpt = "G",
            //        EBChdOpt = "D",
            //        SellPer = 10,
            //        FromDay = 1,
            //        ToDay = 99,
            //        SellCur = "EUR",
            //        ContSpo = "Y",
            //        EarlyAppType = "O",
            //        LongStayType = "N",
            //        LongContDay = 10
            //    }
            //};

            //try
            //{
            //    var ss = sej.SendContract(ContractHeaders, ContractRoomPrice, new ChdPrice[0], ContractOptions,
            //        ContractSpos,
            //        "MOUZ", "FIBULATEST", "N");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

        }

        private void cbHotels_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bEnabled)
            {
                if (Token == Guid.Empty)
                {
                    MessageBox.Show("Please connect first!");
                    cbCostOfferList.DataSource = null;
                    cbCostOfferList.Text = "";
                    return;
                }
                try
                {
                    var costofferlist = xmlgate.GetCostOffers(Token, DateTime.Now, (int)cbHotels.SelectedValue,
                  (int)cbMarkets.SelectedValue);
                    cbCostOfferList.DataSource = null;
                    cbCostOfferList.Text = "";
                    //costofferlist = Methods.GetCostofferList(Convert.ToInt32(cbHotels.SelectedValue));
                    cbCostOfferList.DataSource = costofferlist;
                    cbCostOfferList.ValueMember = "Id";
                    cbCostOfferList.DisplayMember = "Title";

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void CreateContract()
        {
            //var SendContractRequest = new sc.SendContractRequest();
            //var ContractHeaders = new Header[]
            //{
            //    new Header()
            //    {
            //        SupplierName = "MOUZENIDIS TRAVEL GREECE",
            //        Hotel = "ALPHA",
            //        HotelName = "ALPHA HOTEL",
            //        Operator = "FIBULA ROM",
            //        OperatorName = "FIBULA ROMANIA",
            //        BegDate = new DateTime(2017, 04, 01),
            //        EndDate = new DateTime(2017, 10, 31),
            //        Currency = "EUR",
            //        PriceOpt = 1,
            //        ResBegDate = new DateTime(2016, 11, 01),
            //        ResEndDate = new DateTime(2017, 10, 31),
            //        PriceType = "C",
            //        Remark = "TEST"


            //    }
            //};
            //var ContractRoomPrice = new RoomPrice[]
            //{
            //     new RoomPrice()
            //        {
            //            Accom = "DOUBLE",
            //            AccomName = "DOUBLE ROOM",
            //            Room = "STD",
            //            RoomName = "STANDARD ROOM",
            //            Board = "HB",
            //            BoardName = "HALF BOARD",
            //            BegDate = new DateTime(2017, 04, 01),
            //            EndDate = new DateTime(2017, 04, 30),
            //            AdultPrice = 100,
            //            ExtBedPrice = 50,
            //            BoardPrice = 10,
            //            PriceOpt = "Y",
            //            PriceType = "O",
            //            StdAdl = 2,
            //            MinAdl = 2,
            //            MaxAdl = 3,
            //            MaxChd = 0,
            //            MaxPax = 3
            //        },

            //};
            //var ContractChildPrice = new ChdPrice[]
            //{

            //};

            //var ContractOptions = new Option[]
            //{
            //      new Option()
            //        {
            //            Room = "STD",
            //            BegDate = new DateTime(2017, 04, 01),
            //            EndDate = new DateTime(2017, 04, 30),
            //            ReleaseDay = 5
            //        },
            //};
            //var ContractSpos = new SPO[]
            //{
            //    new SPO()
            //    {
            //        SpoNo = 1,
            //        SpoType = "EB",
            //        SellBegDate = Convert.ToDateTime("2016-11-01"),
            //        SellEndDate = Convert.ToDateTime("2017-10-31"),
            //        Accom = "DBL",
            //        Room = "STD",
            //        Board = "HB",
            //        ResBegDate = Convert.ToDateTime("2017-04-01"),
            //        ResEndDate = Convert.ToDateTime("2017-04-30"),
            //        XStay = 0,
            //        YPay = 0,
            //        Price = 100,
            //        ExtBedPrice = 20,
            //        BoardPrice = 0,
            //        PriceOpt = 1,
            //        Remark = "TEST EB",
            //        PriceType = "O",
            //        DaySpoType = 1,
            //        EBSellPer = 10,
            //        EBSellPrice = 0,
            //        SpoOpt = "G",
            //        EBChdOpt = "D",
            //        SellPer = 10,
            //        FromDay = 1,
            //        ToDay = 99,
            //        SellCur = "EUR",
            //        ContSpo = "Y",
            //        EarlyAppType = "O",
            //        LongStayType = "N",
            //        LongContDay = 10
            //    }
            //};

            //try
            //{
            //    var ss = sej.SendContract(ContractHeaders, ContractRoomPrice, new ChdPrice[0], ContractOptions,
            //        ContractSpos,
            //        "MOUZ", "FIBULATEST", "N");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private bool SendPriceOffer(int priceOfferId, int xmlVersion)
        {
            HotelData hotelData = Methods.GetPriceOfferData(priceOfferId, tbLogin.Text, tbPass.Text);
           

           var roomPrices = new List<RoomPrice>();
           var hotelRoomData = new List<HotelRoomData>();
           var hotelRoomChildrenData = new List<HotelRoomChildrenData>();
           var contractChildrenPrices = new List<ChdPrice>();

            var contractHeaders = new Header[]
            {
                new Header()
                {
                    SupplierName = hotelData.priceOfferData.SupplierTitle,
                    Hotel = hotelData.priceOfferData.HotelCode,
                    HotelName = hotelData.priceOfferData.HotelTitle,
                    Operator = hotelData.priceOfferData.Customer,
                    OperatorName = hotelData.priceOfferData.CustomerTitle,
                    BegDate = hotelData.priceOfferData.StayDateBegin,
                    EndDate = hotelData.priceOfferData.StayDateEnd,
                    PriceOpt = hotelData.priceOfferData.PriceOption,
                    ResBegDate = hotelData.priceOfferData.SaleDateBegin,
                    ResEndDate = hotelData.priceOfferData.SaleDateEnd,
                    Currency = hotelData.priceOfferData.Currency,
                    PriceType = hotelData.priceOfferData.PriceType,
                    Remark = hotelData.priceOfferData.Remark
                }
            };

            foreach (var hotelRoom in hotelData.hotelRooms)
            {
                hotelRoomData = hotelData.hotelRoomData.Where(x => x.RoomId == hotelRoom.Id).ToList();
                hotelRoomChildrenData = hotelData.hotelRoomChildrenData.Where(x => x.RoomId == hotelRoom.Id).ToList();

                foreach (var mainHotelRoom in hotelRoomData.OrderBy(x => x.Nth))
                {
                    if (mainHotelRoom.AgeTo < 99)
                        continue;
                    double mainPrice =
                        (double)hotelRoomData.Where(x => x.Nth <= mainHotelRoom.Nth).Sum(x => x.PriceValue);
                    int adultCount = hotelRoom.MinAdult + mainHotelRoom.Nth;
                    int childrenCount = hotelRoom.MaxPax - adultCount;

                    string accomName = @"", accomNameFull = @"", roomName = @"";
                    string room = hotelRoom.RoomTypeId.ToString() + "-" + hotelRoom.RoomCategoryId.ToString();
                    if (xmlVersion == 0)
                    {
                        accomName = adultCount.ToString() + "Adults" + @"+" +
                                        childrenCount.ToString() + @"Children";
                        accomNameFull = accomName;
                        roomName = hotelRoom.Room;
                    }
                    else if (xmlVersion == 1)
                    {
                        accomName = @"** Pax_" + adultCount.ToString() + @"_" + adultCount.ToString() + "_" + adultCount.ToString() + @" **";
                        //accomNameFull = @"Pax_" + hotelRoom.MinAdult.ToString()
                        //            + @"_" + hotelRoom.MaxAdult.ToString()
                        //            + @"_" + hotelRoom.MinChildren.ToString()
                        //            + @"_" + hotelRoom.MaxChildren.ToString()
                        //            + @"_" + hotelRoom.MinPax.ToString()
                        //            + @"_" + hotelRoom.MaxPax.ToString();
                        accomNameFull = accomName;
                        if (hotelRoom.Room.Length > 36)
                            hotelRoom.Room = hotelRoom.Room.Substring(0, 35);
                        int minChildren = 0;
                        minChildren = hotelRoom.MinPax - adultCount < 1 ? 0 : hotelRoom.MinPax - adultCount;
                        if (minChildren > hotelRoom.MaxChildren)
                            minChildren = hotelRoom.MaxChildren;

                      
                        roomName = hotelRoom.Room + @"_" + minChildren.ToString()
                             + @"_" + childrenCount.ToString();
                    }
                   
                    //+ @"-" +
                                                                                                              //hotelRoom.PansionId.ToString();
                    // + @"-" + hotelRoom.Pansion;
                    roomPrices.Add(new RoomPrice()
                    {
                        Room = room,
                        RoomName = roomName,
                        Accom = accomName,
                        AccomName = accomNameFull,
                        Board = hotelRoom.PansionId.ToString(),
                        BoardName = hotelRoom.Pansion,
                        BegDate = hotelRoom.StayFrom,
                        EndDate = hotelRoom.StayTo,
                        AdultPrice = mainPrice,
                        ExtBedPrice = 0,
                        BoardPrice = 0,
                        PriceOpt = "Y",
                        PriceType = "O",
                        StdAdl = adultCount,
                        MinAdl = adultCount,
                        MaxAdl = adultCount,
                        MaxChd = childrenCount,
                        MaxPax = hotelRoom.MaxPax
                    });


                    foreach (var hotelRoomChildrenDataItem in hotelRoomChildrenData.Where(x => x.ChildrenCount <= childrenCount))
                    {
                        contractChildrenPrices.Add(new ChdPrice()
                        {
                            BegDate = hotelRoom.StayFrom,
                            EndDate = hotelRoom.StayTo,
                            Room = room,
                            Board = hotelRoom.PansionId.ToString(),
                            Accom = accomName,
                            Adl = adultCount,
                            C1Age1 = hotelRoomChildrenDataItem.Ch1AgeFrom,
                            C1Age2 = hotelRoomChildrenDataItem.Ch1AgeTo,
                            C1Per = hotelRoomChildrenDataItem.Ch1AgeFrom == -1 ? -1 : 0,
                            C1Price = hotelRoomChildrenDataItem.Ch1PriceValue,
                            C2Age1 = hotelRoomChildrenDataItem.Ch2AgeFrom,
                            C2Age2 = hotelRoomChildrenDataItem.Ch2AgeTo,
                            C2Per = hotelRoomChildrenDataItem.Ch2AgeFrom == -1 ? -1 : 0,
                            C2Price = hotelRoomChildrenDataItem.Ch2PriceValue,
                            C3Age1 = hotelRoomChildrenDataItem.Ch3AgeFrom,
                            C3Age2 = hotelRoomChildrenDataItem.Ch3AgeTo,
                            C3Per = hotelRoomChildrenDataItem.Ch3AgeFrom == -1 ? -1 : 0,
                            C3Price = hotelRoomChildrenDataItem.Ch3PriceValue,
                            C4Age1 = hotelRoomChildrenDataItem.Ch4AgeFrom,
                            C4Age2 = hotelRoomChildrenDataItem.Ch4AgeTo,
                            C4Per = hotelRoomChildrenDataItem.Ch4AgeFrom == -1 ? -1 : 0,
                            C4Price = hotelRoomChildrenDataItem.Ch4PriceValue,
                        });
                    }
                }
            }


            var contractRoomPrices = roomPrices.ToArray();

            ContractHeader[] ContractHeaders = new ContractHeader[1];

            try
            {
               
                var ss = tourvisioClient.SendContract(
                    contractHeaders, 
                    contractRoomPrices, 
                    contractChildrenPrices.ToArray(),
                    new Option[0],
                    new SPO[0],
                    //"MOUZ", "FIBULATEST", "N");
                    "MOU", "12345", "N");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return (true);
        }
  
        private void cbCostOfferList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //label3.Text = cbCostOfferList.SelectedValue.ToString();
        }

        private void btnSendContract__Click(object sender, EventArgs e)
        {

            try
            {
                var ContractHeader = new Header()
                {
                    SupplierName = pd.ContractHeader.SupplierName,
                    Hotel = pd.ContractHeader.Hotel,
                    HotelName = pd.ContractHeader.HotelName,
                    Operator = pd.ContractHeader.Operator,
                    OperatorName = pd.ContractHeader.OperatorName,
                    BegDate = pd.ContractHeader.BegDate,
                    EndDate = pd.ContractHeader.EndDate,
                    PriceOpt = pd.ContractHeader.PriceOpt,
                    ResBegDate = pd.ContractHeader.ResBegDate,
                    ResEndDate = pd.ContractHeader.ResEndDate,
                    Currency = pd.ContractHeader.Currency,
                    PriceType = pd.ContractHeader.PriceType,
                    Remark = pd.ContractHeader.Remark
                };
                var ContractRoomData = pd.contractRoomData.Select(x => new RoomPrice()
                {
                    Room = x.Room,
                    RoomName = x.RoomName,
                    Accom = x.Accom,
                    AccomName = x.AccomName,
                    Board = x.Board,
                    BoardName = x.BoardName,
                    BegDate = x.BegDate,
                    EndDate = x.EndDate,
                    AdultPrice = Convert.ToDouble(x.AdultPrice),
                    ExtBedPrice = Convert.ToDouble(x.ExtBedPrice),
                    BoardPrice = Convert.ToDouble(x.BoardPrice),
                    PriceOpt = x.PriceOpt,
                    PriceType = x.PriceType,
                    StdAdl = x.StdAdl,
                    MinAdl = x.MinAdl,
                    MaxAdl = x.MaxAdl,
                    MaxChd = x.MaxChd,
                    MaxPax = x.MaxPax
                }).ToArray();

                var ContractRoomChildrenData = pd.contractChildPriceContents.Select(x => new ChdPrice()
                {
                    BegDate = x.BegDate,
                    EndDate = x.EndDate,
                    Room = x.Room,
                    Board = x.Board,
                    Accom = x.Accom,
                    Adl = x.Adl,
                    C1Age1 = x.C1Age1,
                    C1Age2 = x.C1Age2,
                    C1Per = x.C1Per,
                    C1Price = Convert.ToDouble(x.C1Price),
                    C2Age1 = x.C2Age1,
                    C2Age2 = x.C2Age2,
                    C2Per = x.C2Per,
                    C2Price = Convert.ToDouble(x.C2Price),
                    C3Age1 = x.C3Age1,
                    C3Age2 = x.C3Age2,
                    C3Per = x.C3Per,
                    C3Price = Convert.ToDouble(x.C3Price),
                    C4Age1 = x.C4Age1,
                    C4Age2 = x.C4Age2,
                    C4Per = x.C4Per,
                    C4Price = Convert.ToDouble(x.C4Price)
                }).ToArray();
                var ContractOptionData = pd.options.Select(x => new Option()
                {
                    BegDate = x.BegDate,
                    EndDate = x.EndDate,
                    Room = x.Room,
                    ReleaseDay = x.ReleaseDay
                }).ToArray();
                var ContractSpoData = pd.spos.Select(x => new SPO()
                {
                    SpoNo = x.SpoNo,
                    SpoType = x.SpoType,
                    SellBegDate = x.SellBegDate,
                    SellEndDate = x.SellEndDate,
                    Room = x.Room,
                    Board = x.Board,
                    ResBegDate = x.ResBegDate,
                    ResEndDate = x.ResEndDate,
                    XStay = x.XStay,
                    YPay = x.YPay,
                    Price = Convert.ToDouble(x.Price),
                    ExtBedPrice = Convert.ToDouble(x.ExtBedPrice),
                    BoardPrice = Convert.ToDouble(x.BoardPrice),
                    PriceOpt = x.PriceOpt,
                    Remark = x.Remark,
                    PriceType = x.PriceType,
                    DaySpoType = x.DaySpoType,
                    EBSellPer = Convert.ToDouble(x.EBSellPer),
                    EBSellPrice = Convert.ToDouble(x.EBSellPrice),
                    SpoOpt = x.SpoOpt,
                    EBChdOpt = x.EBChdOpt,
                    SellPer = Convert.ToDouble(x.SellPer),
                    FromDay = x.FromDay,
                    ToDay = x.ToDay,
                    SellCur = x.SellCur,
                    ContSpo = x.ContSpo,
                    EarlyAppType = x.EarlyAppType,
                    LongStayType = x.LongStayType,
                    LongContDay = x.LongContDay,
                }).ToArray();

                //var ss = sej.SendContract(
                //                             new Header[] { ContractHeader } 
                //                            ,ContractRoomData
                //                            ,ContractRoomChildrenData
                //                            ,ContractOptionData
                //                            ,ContractSpoData
                //                            , "MOUZ"
                //                            , "FIBULATEST"
                //                            , "N"
                //                         );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            hotelData = Methods.GetPriceOfferData((int)cbCostOfferList.SelectedValue, tbLogin.Text, tbPass.Text);
            dgRooms.DataSource = hotelData.hotelRooms;
            dgRooms.AutoResizeColumns();
            dgRoomsData.DataSource = hotelData.hotelRoomData;
            dgRoomsData.AutoResizeColumns();
        }

        private void btnPrepareContract_Click(object sender, EventArgs e)
        {
            var roomPrices = new List<RoomPrice>();
            List<HotelRoomData> hotelRoomData = new List<HotelRoomData>();
            List<HotelRoomChildrenData> hotelRoomChildrenData = new List<HotelRoomChildrenData>();
            var contractChildrenPrices = new List<ChdPrice>();

            var contractHeaders = new Header[]
            {
                new Header()
                {
                    SupplierName = hotelData.priceOfferData.SupplierTitle,
                    Hotel = hotelData.priceOfferData.HotelCode,
                    HotelName = hotelData.priceOfferData.HotelTitle,
                    Operator = hotelData.priceOfferData.Customer,
                    OperatorName = hotelData.priceOfferData.CustomerTitle,
                    BegDate = hotelData.priceOfferData.StayDateBegin,
                    EndDate = hotelData.priceOfferData.StayDateEnd,
                    PriceOpt = hotelData.priceOfferData.PriceOption,
                    ResBegDate = hotelData.priceOfferData.SaleDateBegin,
                    ResEndDate = hotelData.priceOfferData.SaleDateEnd,
                    Currency = hotelData.priceOfferData.Currency,
                    PriceType = hotelData.priceOfferData.PriceType,
                    Remark = hotelData.priceOfferData.Remark
                }
            };

            foreach (var hotelRoom in hotelData.hotelRooms)
            {
                hotelRoomData = hotelData.hotelRoomData.Where(x=>x.RoomId == hotelRoom.Id).ToList();
                hotelRoomChildrenData = hotelData.hotelRoomChildrenData.Where(x => x.RoomId == hotelRoom.Id).ToList();

                foreach (var mainHotelRoom in hotelRoomData.OrderBy(x=>x.Nth))
                {
                    if (mainHotelRoom.AgeTo < 99)
                        continue;
                    double mainPrice =
                        (double)hotelRoomData.Where(x => x.Nth <= mainHotelRoom.Nth).Sum(x => x.PriceValue);
                    int adultCount = hotelRoom.MinAdult + mainHotelRoom.Nth;
                    int childrenCount = hotelRoom.MaxPax - adultCount;

                    string accomName = adultCount.ToString() + "Adults" + @"+" +
                                       childrenCount.ToString() + @"Children";
                    string room = hotelRoom.RoomTypeId.ToString() + "-" + hotelRoom.RoomCategoryId.ToString();//+ @"-" +
                                  //hotelRoom.PansionId.ToString();
                    string roomName = hotelRoom.Room;// + @"-" + hotelRoom.Pansion;
                    roomPrices.Add(new RoomPrice()
                    {
                        Room = room,
                        RoomName = roomName,
                        Accom = accomName,
                        AccomName = accomName,
                        Board = hotelRoom.PansionId.ToString(),
                        BoardName = hotelRoom.Pansion,
                        BegDate = hotelRoom.StayFrom,
                        EndDate = hotelRoom.StayTo,
                        AdultPrice = mainPrice,
                        ExtBedPrice = 0,
                        BoardPrice = 0,
                        PriceOpt = "Y",
                        PriceType = "O",
                        StdAdl = adultCount,
                        MinAdl = adultCount,
                        MaxAdl = adultCount,
                        MaxChd = childrenCount,
                        MaxPax = hotelRoom.MaxPax
                    });


                    foreach (var hotelRoomChildrenDataItem in hotelRoomChildrenData.Where(x=>x.ChildrenCount <= childrenCount))
                    {
                        contractChildrenPrices.Add(new ChdPrice()
                        {
                            BegDate = hotelRoom.StayFrom,
                            EndDate = hotelRoom.StayTo,
                            Room = room,
                            Board = hotelRoom.PansionId.ToString(),
                            Accom = accomName,
                            Adl = adultCount,
                            C1Age1 = hotelRoomChildrenDataItem.Ch1AgeFrom,
                            C1Age2 = hotelRoomChildrenDataItem.Ch1AgeTo,
                            C1Per = hotelRoomChildrenDataItem.Ch1AgeFrom == -1 ? -1 : 0,
                            C1Price = hotelRoomChildrenDataItem.Ch1PriceValue,
                            C2Age1 = hotelRoomChildrenDataItem.Ch2AgeFrom,
                            C2Age2 = hotelRoomChildrenDataItem.Ch2AgeTo,
                            C2Per = hotelRoomChildrenDataItem.Ch2AgeFrom == -1 ? -1 : 0,
                            C2Price = hotelRoomChildrenDataItem.Ch2PriceValue,
                            C3Age1 = hotelRoomChildrenDataItem.Ch3AgeFrom,
                            C3Age2 = hotelRoomChildrenDataItem.Ch3AgeTo,
                            C3Per = hotelRoomChildrenDataItem.Ch3AgeFrom == -1 ? -1 : 0,
                            C3Price = hotelRoomChildrenDataItem.Ch3PriceValue,
                            C4Age1 = hotelRoomChildrenDataItem.Ch4AgeFrom,
                            C4Age2 = hotelRoomChildrenDataItem.Ch4AgeTo,
                            C4Per = hotelRoomChildrenDataItem.Ch4AgeFrom == -1 ? -1 : 0,
                            C4Price = hotelRoomChildrenDataItem.Ch4PriceValue,
                        });
                    }
                }
            }

            
                var contractRoomPrices = roomPrices.ToArray();

                ContractHeader[] ContractHeaders = new ContractHeader[1];

                try
                {
                    var ss = tourvisioClient.SendContract(contractHeaders, contractRoomPrices, contractChildrenPrices.ToArray(),
                        new Option[0],
                        new SPO[0],
                        "MOUZ", "FIBULATEST", "N");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


            }

        private void btnGetStopSalesData_Click(object sender, EventArgs e)
        {
            hotelData = Methods.GetPriceOfferData((int)cbCostOfferList.SelectedValue, tbLogin.Text, tbPass.Text);
            dgRooms.DataSource = hotelData.hotelRooms;
            dgRooms.AutoResizeColumns();
            dgRoomsData.DataSource = hotelData.hotelRoomData;
            dgRoomsData.AutoResizeColumns();
        }

        private void cbRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bEnabled)
            {
                hotels = Methods.GetHotels((int)cbRegions.SelectedValue);
                cbHotels.DataSource = hotels;
                cbHotels.DisplayMember = "Title";
                cbHotels.ValueMember = "Id";
            }
        }

        private void btnSendRegionContracts_Click(object sender, EventArgs e)
        {
            foreach (var hotel in hotels)
            {
               
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                var con = xmlgate.Connect(tbLogin.Text, tbPass.Text);
                Token = new Guid(con.Guid);
                lToken.Text = @"Token: " + Token.ToString();
               
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid authorization data!");
            }           
        }

        private void btnGetPartnerHotelList_Click(object sender, EventArgs e)
        {
            bEnabled = false;
            PartnerId = Methods.ValidateSecurityToken(Token.ToString());
            if (PartnerId == 0)
            {
                MessageBox.Show("Unknown partner");
                return;
            }
            hotels = Methods.GetPartnerHotels(PartnerId);
            cbHotels.DataSource = null;
            cbHotels.Text = "";
            cbHotels.DataSource = hotels;
            cbHotels.DisplayMember = "Title";
            cbHotels.ValueMember = "Id";
            bEnabled = true;
        }

        private void btnSendContracts_Click(object sender, EventArgs e)
        {
            foreach (var hotel in hotels.Where(x=>x.Id > 0))
            {

                var spos = xmlgate.GetCostOffers(Token, DateTime.Now, hotel.Id, (int) cbMarkets.SelectedValue);
                if ((int) cbPriceOfferTypes.SelectedValue == 0 || (int) cbPriceOfferTypes.SelectedValue == 1)
                {
                    foreach (
                        var spo in
                            spos.Where(x => x.costOfferProperties.Any(y => y.Id == 1)).OrderBy(z => z.PriorityOrder))
                    {
                        SendPriceOffer(spo.Id, 1);
                        lStatus.Text = hotel.Title + @": " + spo.Title;
                        lStatus.Refresh();
                    }
                }

                if ((int) cbPriceOfferTypes.SelectedValue == 0 || (int) cbPriceOfferTypes.SelectedValue == 2)
                {
                    foreach (
                        var spo in
                            spos.Where(x => x.costOfferProperties.Any(y => y.Id != 1)).OrderBy(z => z.PriorityOrder))
                    {
                        SendPriceOffer(spo.Id, 1);
                        lStatus.Text = hotel.Title + @": " + spo.Title;
                        lStatus.Refresh();
                    }
                }
            }
        }

        private void cbPartners_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var partner = partners.First(x => x.Id == (int)cbPartners.SelectedValue);
            if (partner == null)
                return;
            tbLogin.Text = partner.Login;
            tbPass.Text = partner.Password;
            cbMarkets.SelectedValue = partner.MarketId;
        }
    }
    }


