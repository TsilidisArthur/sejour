﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sejour
{
  

   public class Item
	{
		public int Id { get; set; }
		public string Title { get; set; }
	}

	public class HotelRoom
	{
		public int Id { get; set; }
		public int RoomTypeId {get;set;}
		public int RoomCategoryId { get; set; }
		public int PansionId { get; set; }
		public string Room { get; set; }
		public string Pansion { get; set; }
		public DateTime StayFrom { get; set; }
		public DateTime StayTo { get; set; }
		public int MinPax { get; set; }
		public int MaxPax { get; set; }
		public int MinAdult { get; set; }
		public int MaxAdult { get; set; }
		public int MinChildren { get; set; }
		public int MaxChildren { get; set; }
	}

	public class HotelRoomData
	{
		public int Id { get; set; }
		public int RoomId { get; set; }
		public int AgeFrom { get; set; }
		public int AgeTo { get; set; }
		public int Nth { get; set; }
		public decimal PriceValue { get; set; }
		public bool MainAccomodation { get; set; }
	}

	public class HotelRoomChildrenData
	{
		public int RoomId { get; set; }
		public int ChildrenCount { get; set; }
		public int Ch1AgeFrom { get; set; }
		public int Ch1AgeTo { get; set; }
		public double Ch1PriceValue { get; set; }
		public int Ch2AgeFrom { get; set; }
		public int Ch2AgeTo { get; set; }
		public double Ch2PriceValue { get; set; }
		public int Ch3AgeFrom { get; set; }
		public int Ch3AgeTo { get; set; }
		public double Ch3PriceValue { get; set; }
		public int Ch4AgeFrom { get; set; }
		public int Ch4AgeTo { get; set; }
		public double Ch4PriceValue { get; set; }
	}

	public class PriceOfferData
	{
		public string SupplierTitle { get; set; }
		public string HotelCode { get; set; }
		public string HotelTitle { get; set; }
		public DateTime SaleDateBegin { get; set; }
		public DateTime SaleDateEnd { get; set; }
		public string Customer { get; set; }
		public string CustomerTitle { get; set; }
		public string Currency { get; set; }
		public string PriceType { get; set; }
		public string Remark { get; set; }
		public int PriceOption { get; set; }
		public DateTime StayDateBegin { get; set; }
		public DateTime StayDateEnd { get; set; }
}

	public class HotelData
	{
		public AuthorizationData authorizationData { get; set; }
		public PriceOfferData priceOfferData { get; set; }
		public List<HotelRoom> hotelRooms { get; set; }
		public List<HotelRoomData> hotelRoomData { get; set; }  
		public List<HotelRoomChildrenData> hotelRoomChildrenData { get; set; }
	}

	public class AuthorizationData
	{
		public string Login { get; set; }
		public string Password { get; set; }
	}
}
