﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace sejour.DataAccessLayer
{
    class DbHelpers
    {
        public static DataTable ExecuteAndReturnTable(string query) //, out bool status, out string exception)
        {
            SqlConnection cn = new SqlConnection(ConnectionString.Value);
            SqlCommand com = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable("dt");
            com.Connection = cn;
            com.Parameters.Clear();
            com.CommandText = query;
            com.CommandType = CommandType.Text;
            da.SelectCommand = com;
            try
            {

                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.StartsWith(@"xmlgate: ") ? ex.Message : @"Unknown system error");
            }
            finally
            {
                if (cn.State != ConnectionState.Closed)
                {
                    cn.Close();
                }
            }
            return (dt);
        }

        public static DataSet ExecuteAndReturnDataSet(string query)
        {
            SqlConnection cn = new SqlConnection(ConnectionString.Value);
            SqlCommand com = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet("ds");
            com.Connection = cn;
            com.Parameters.Clear();
            com.CommandText = query;
            com.CommandType = CommandType.Text;
            da.SelectCommand = com;
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.StartsWith(@"xmlgate: ") ? ex.Message : @"Unknown system error");
            }
            finally
            {
                if (cn.State != ConnectionState.Closed)
                {
                    cn.Close();

                }
            }
            return (ds);
        }

        public static object ExecuteAndReturnValue(string query)
        {
            object oRet = 1;
            SqlConnection cn = new SqlConnection(ConnectionString.Value);
            SqlCommand com = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            com.Connection = cn;
            com.Parameters.Clear();
            com.CommandText = query;
            com.CommandType = CommandType.Text;
            da.SelectCommand = com;

            try
            {
                cn.Open();
                oRet = com.ExecuteScalar();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (cn.State != ConnectionState.Closed)
                {
                    cn.Close();

                }
            }
            return (oRet);
        }

        public static DataSet GetHotelAvailability(int HotelId, int RoomTypeId, int RoomCategoryId, int PansionId, int AccomodationId, int TouroperatorKey,
                                                DateTime StayFrom, DateTime StayTo, DataTable Clients)
        {
            SqlConnection cn = new SqlConnection(ConnectionString.Value);
            SqlCommand com = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet("ds");
            com.Connection = cn;
            com.Parameters.Clear();
            com.CommandText = @"_cost_get_availability";
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ServiceTypeKey", 1);
            com.Parameters.AddWithValue("@ServiceKey", HotelId);
            com.Parameters.AddWithValue("@Subcode1", RoomTypeId);
            com.Parameters.AddWithValue("@Subcode2", PansionId);
            com.Parameters.AddWithValue("@Subcode3", RoomCategoryId);
            com.Parameters.AddWithValue("@Subcode4", AccomodationId);
            com.Parameters.AddWithValue("@TouroperatorKey", TouroperatorKey);
            com.Parameters.AddWithValue("@MarketKey", -1);
            com.Parameters.AddWithValue("@DateFrom", StayFrom);
            com.Parameters.AddWithValue("@DateTo", StayTo);
            com.Parameters.AddWithValue("@Pax", Clients);
            da.SelectCommand = com;
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (cn.State != ConnectionState.Closed)
                {
                    cn.Close();

                }
            }
            return (ds);

        }
    }

    public static class ConnectionString
    {
        public static string Value { get; set; }
    }
}
