﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using sejour;
using sejour.DataAccessLayer;
using System.Data.SqlClient;
using System.Runtime.Remoting.Metadata.W3cXsd2001;


namespace sejour.DataAccessLayer
{
	public static class Methods
	{

		public static sejour.HotelData GetPriceOfferData(int PriceofferId, string Login, string Password)
		{
			ilws._xmlgateClient xClient = new ilws._xmlgateClient();
			var connection = xClient.Connect(Login, Password);
			sejour.HotelData hotelData = new HotelData();
			SqlConnection cn = new SqlConnection(ConnectionString.Value);
			SqlCommand com = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataSet ds = new DataSet("ds");
			com.Connection = cn;
			com.Parameters.Clear();
			com.CommandText = @"_core_getspopricedata_tourvisio";
			com.Parameters.Clear();
			com.Parameters.AddWithValue("@ConnectionToken", connection.Guid);
			com.Parameters.AddWithValue("@SpoKey", PriceofferId);
			com.Parameters.AddWithValue("@only_mine", 1);
			com.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = com;
			try
			{

				da.Fill(ds);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message.StartsWith(@"xmlgate: ") ? ex.Message : @"Unknown system error");
			}
			finally
			{
				if (cn.State != ConnectionState.Closed)
				{
					cn.Close();
				}
			}

			sejour.PriceOfferData priceOfferData =
				ds.Tables[0].AsEnumerable().Select(x => new sejour.PriceOfferData()
				{
					SupplierTitle = x.Field<string>("supplier_title"),
					HotelCode = x.Field<string>("hotel_code"),
					HotelTitle = x.Field<string>("hotel_title"),
					SaleDateBegin = x.Field<DateTime>("salebegin_date"),
					SaleDateEnd = x.Field<DateTime>("saleend_date"),
					Customer = x.Field<string>("customer"),
					CustomerTitle = x.Field<string>("customer_title"),
					Currency = x.Field<string>("currency"),
					PriceType = x.Field<string>("price_type"),
					Remark = x.Field<string>("remark"),
					PriceOption = x.Field<int>("price_option"),
					StayDateBegin = x.Field<DateTime>("staybegin_date"),
					StayDateEnd = x.Field<DateTime>("stayend_date")
				}
					).ToList().FirstOrDefault();
	
			
			List<sejour.HotelRoom> hotelRooms = 
				ds.Tables[1].AsEnumerable().Select(x => new sejour.HotelRoom()
				{
					Id = x.Field<int>("Id"),
					RoomTypeId = x.Field<int>("roomtype_id"),
					RoomCategoryId = x.Field<int>("roomcategory_id"),
					PansionId = x.Field<int>("pansion_id"),
					Room = x.Field<string>("room"),
					Pansion = x.Field<string>("pansion"),
					StayFrom = x.Field<DateTime>("staybegin_date"),
					StayTo = x.Field<DateTime>("stayend_date"),
					MinPax = x.Field<int>("min_pax"),
					MaxPax = x.Field<int>("max_pax"),
					MinAdult = x.Field<int>("min_adult"),
					MaxAdult = x.Field<int>("max_adult"),
					MinChildren = x.Field<int>("min_children"),
					MaxChildren = x.Field<int>("max_children")
				}).ToList();
			int index = 0;

			List<sejour.HotelRoomData> hotelRoomData =
			  ds.Tables[2].AsEnumerable().Select(x => new sejour.HotelRoomData()
			  {
				  Id = x.Field<int>("id"),
				  RoomId = x.Field<int>("room_id"),
				  AgeFrom = x.Field<int>("age_from"),
				  AgeTo = x.Field<int>("age_to"),
				  Nth = x.Field<int>("nth"),
				  PriceValue = x.Field<decimal>("price_value"),
				  MainAccomodation = x.Field<bool>("main_accomodation")

			}).ToList();

			List<sejour.HotelRoomChildrenData> hotelRoomChildrenData =
				ds.Tables[3].AsEnumerable().Select(x => new sejour.HotelRoomChildrenData()
				{
					RoomId = x.Field<int>("room_id"),
					ChildrenCount = x.Field<int>("children_count"),
					Ch1AgeFrom = x.Field<int>("ch1age_from"),
					Ch1AgeTo = x.Field<int>("ch1age_to"),
					Ch1PriceValue = (double)x.Field<decimal>("ch1price_value"),
					Ch2AgeFrom = x.Field<int>("ch2age_from"),
					Ch2AgeTo = x.Field<int>("ch2age_to"),
					Ch2PriceValue = (double)x.Field<decimal>("ch2price_value"),
					Ch3AgeFrom = x.Field<int>("ch3age_from"),
					Ch3AgeTo = x.Field<int>("ch3age_to"),
					Ch3PriceValue = (double)x.Field<decimal>("ch3price_value"),
					Ch4AgeFrom = x.Field<int>("ch4age_from"),
					Ch4AgeTo = x.Field<int>("ch4age_to"),
					Ch4PriceValue = (double)x.Field<decimal>("ch4price_value")

				}).ToList();
			hotelData.priceOfferData = priceOfferData;
			hotelData.hotelRooms = hotelRooms;
			hotelData.hotelRoomData = hotelRoomData;
			hotelData.hotelRoomChildrenData = hotelRoomChildrenData;
			return (hotelData);
		}

		public static List<Hotel> GetHotels(int regionId)
		{
			string Query = @"select _pr.PR_KEY as Id, _pr.pr_name + ' (' + _ct.CT_NAME + ')' as Title
								from partners _pr
								inner
								join partnersbytype _pbt on _pbt.pb_prkey = _pr.pr_key
								inner
								join citydictionary _ct on _ct.ct_key = _pr.pr_ctkey
								where _pbt.pb_ptkey = 1 and _pr.pr_cohkey in (select coh_key from CATEGORIESOFHOTEL where COH_GLOBALKEY in (47)) ";
			if (regionId > -1)
				Query += @" and _ct.ct_rgkey = " + regionId.ToString();
			Query += @" order by Title ";
			DataTable dt = DbHelpers.ExecuteAndReturnTable(Query);
			List<Hotel> hotels =
				dt.AsEnumerable().Select(x => new Hotel()
				{
					Id = x.Field<int>("Id"),
					Title = x.Field<string>("Title")
				}).ToList();
			return (hotels);
		}

		public static List<Hotel> GetPartnerHotels(int PartnerId)
		{
			string Query = @"select _pr.PR_KEY as Id, _pr.pr_name + ' (' + _ct.CT_NAME + ')' as Title
								from partners _pr
								inner join partnersbytype _pbt on _pbt.pb_prkey = _pr.pr_key
								inner join citydictionary _ct on _ct.ct_key = _pr.pr_ctkey
								inner join _partners_exchangeHotelsList hl on hl.HotelId = _pr.pr_key
								where _pbt.pb_ptkey = 1 
										and hl.PartnerId = " + PartnerId.ToString() + @"
										and _pr.pr_cohkey in (select coh_key from CATEGORIESOFHOTEL where COH_GLOBALKEY in (47)) 
								union 
								select -1 as Id, ' < All > ' as title ";
			Query += @" order by Title ";
			DataTable dt = DbHelpers.ExecuteAndReturnTable(Query);
			var hotels =
				dt.AsEnumerable().Select(x => new Hotel()
				{
					Id = x.Field<int>("Id"),
					Title = x.Field<string>("Title")
				}).ToList();
			return (hotels);
		}

		public static List<CostofferList> GetCostofferList(int HotelId)
		{
			string Query = @"select gs_key as Id, gs_title as Title
								from _cost_groupcostoffers _gco
								inner join _cost_pricegroups _pg on _pg.pg_key = _gco.gs_grkey
								where _gco.gs_htkey = " + HotelId.ToString() + @" 
								and GETDATE() < gs_saledateto and _pg.pg_type = 1
								order by gs_key  ";
			DataTable dt = DbHelpers.ExecuteAndReturnTable(Query);
			List<CostofferList> costofferlist =
				dt.AsEnumerable().Select(x => new CostofferList()
				{
					Id = x.Field<int>("Id"),
					Title = x.Field<string>("Title")
				}).ToList();
			return (costofferlist);
		}

		public static List<Item> GetMarkets()
		{
			string Query = @"select mr_key as Id, mr_name as Title from market where mr_key > 99 order by mr_name";
			DataTable dt = DbHelpers.ExecuteAndReturnTable(Query);
			List<Item> markets =
				dt.AsEnumerable().Select(x => new Item()
				{
					Id = x.Field<int>("Id"),
					Title = x.Field<string>("Title")
				}).ToList();
			return (markets);
		}

		public static int ValidateSecurityToken(string Token)
		{
			string Query = @" declare @PartnerId int = 0
							  select top 1 @PartnerId = PartnerKey 
							  from _core_connections where ConnectionGuid = '" + Token + @"'
							  select @PartnerId as PartnerId";
			return((int)DbHelpers.ExecuteAndReturnValue(Query));
		}

		public static List<Item> GetHotelRegions()
		{
			string Query = @"select rg_key as Id, cn_name + ': ' + rg_name as Title 
							from region rg
							inner join COUNTRY cn on cn.CN_KEY = rg.rg_cnkey
							where rg_key > 0 and rg_cnkey in (4,20) 
							union 
							select -1 as Id, ' < All >' as title
							order by title";
			DataTable dt = DbHelpers.ExecuteAndReturnTable(Query);
			List<Item> markets =
				dt.AsEnumerable().Select(x => new Item()
				{
					Id = x.Field<int>("Id"),
					Title = x.Field<string>("Title")
				}).ToList();
			return (markets);
		}

		public static sejour.HotelData GetPriceOfferData1(int PriceofferId)
		{
			sejour.HotelData hotelData = new HotelData();
			SqlConnection cn = new SqlConnection(ConnectionString.Value);
			SqlCommand com = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataSet ds = new DataSet("ds");
			com.Connection = cn;
			com.Parameters.Clear();
			com.CommandText = @"_core_getspopricedata_tourvisio";
			com.Parameters.Clear();
			com.Parameters.AddWithValue("@ConnectionToken", new Guid("A4B8A6D4-F07F-48AE-B5AB-061F3A84636D"));
			com.Parameters.AddWithValue("@SpoKey", PriceofferId);
			com.Parameters.AddWithValue("@only_mine", 1);
			com.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = com;
			try
			{

				da.Fill(ds);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message.StartsWith(@"xmlgate: ") ? ex.Message : @"Unknown system error");
			}
			finally
			{
				if (cn.State != ConnectionState.Closed)
				{
					cn.Close();
				}
			}

			sejour.PriceOfferData priceOfferData =
				ds.Tables[0].AsEnumerable().Select(x => new sejour.PriceOfferData()
				{
					SupplierTitle = x.Field<string>("supplier_title"),
					HotelCode = x.Field<string>("hotel_code"),
					HotelTitle = x.Field<string>("hotel_title"),
					SaleDateBegin = x.Field<DateTime>("salebegin_date"),
					SaleDateEnd = x.Field<DateTime>("saleend_date"),
					Customer = x.Field<string>("customer"),
					CustomerTitle = x.Field<string>("customer_title"),
					Currency = x.Field<string>("currency"),
					PriceType = x.Field<string>("price_type"),
					Remark = x.Field<string>("remark"),
					PriceOption = x.Field<int>("price_option"),
					StayDateBegin = x.Field<DateTime>("staybegin_date"),
					StayDateEnd = x.Field<DateTime>("stayend_date")
				}
					).ToList().FirstOrDefault();


			List<sejour.HotelRoom> hotelRooms =
				ds.Tables[1].AsEnumerable().Select(x => new sejour.HotelRoom()
				{
					Id = x.Field<int>("Id"),
					RoomTypeId = x.Field<int>("roomtype_id"),
					RoomCategoryId = x.Field<int>("roomcategory_id"),
					PansionId = x.Field<int>("pansion_id"),
					Room = x.Field<string>("room"),
					Pansion = x.Field<string>("pansion"),
					StayFrom = x.Field<DateTime>("staybegin_date"),
					StayTo = x.Field<DateTime>("stayend_date"),
					MinPax = x.Field<int>("min_pax"),
					MaxPax = x.Field<int>("max_pax"),
					MinAdult = x.Field<int>("min_adult"),
					MaxAdult = x.Field<int>("max_adult"),
					MinChildren = x.Field<int>("min_children"),
					MaxChildren = x.Field<int>("max_children")
				}).ToList();
			int index = 0;

			List<sejour.HotelRoomData> hotelRoomData =
			  ds.Tables[2].AsEnumerable().Select(x => new sejour.HotelRoomData()
			  {
				  Id = x.Field<int>("id"),
				  RoomId = x.Field<int>("room_id"),
				  AgeFrom = x.Field<int>("age_from"),
				  AgeTo = x.Field<int>("age_to"),
				  Nth = x.Field<int>("nth"),
				  PriceValue = x.Field<decimal>("price_value"),
				  MainAccomodation = x.Field<bool>("main_accomodation")

			  }).ToList();

			List<sejour.HotelRoomChildrenData> hotelRoomChildrenData =
				ds.Tables[3].AsEnumerable().Select(x => new sejour.HotelRoomChildrenData()
				{
					RoomId = x.Field<int>("room_id"),
					ChildrenCount = x.Field<int>("children_count"),
					Ch1AgeFrom = x.Field<int>("ch1age_from"),
					Ch1AgeTo = x.Field<int>("ch1age_to"),
					Ch1PriceValue = (double)x.Field<decimal>("ch1price_value"),
					Ch2AgeFrom = x.Field<int>("ch2age_from"),
					Ch2AgeTo = x.Field<int>("ch2age_to"),
					Ch2PriceValue = (double)x.Field<decimal>("ch2price_value"),
					Ch3AgeFrom = x.Field<int>("ch3age_from"),
					Ch3AgeTo = x.Field<int>("ch3age_to"),
					Ch3PriceValue = (double)x.Field<decimal>("ch3price_value"),
					Ch4AgeFrom = x.Field<int>("ch4age_from"),
					Ch4AgeTo = x.Field<int>("ch4age_to"),
					Ch4PriceValue = (double)x.Field<decimal>("ch4price_value")

				}).ToList();
			hotelData.priceOfferData = priceOfferData;
			hotelData.hotelRooms = hotelRooms;
			hotelData.hotelRoomData = hotelRoomData;
			hotelData.hotelRoomChildrenData = hotelRoomChildrenData;
			return (hotelData);
		}

		public static List<Partner> GetPartners()
		{
			string Query = @"select distinct 
								 pr_key as Id
								,pr_name as Title
								,pr_mrkey as MarketId
								,loginInfo.[Login]
								,loginInfo.[password] 
							from partners _pr
							outer apply (select pcn_login as [Login], pcn_password as [Password] from Personconnections where pcn_prkey = _pr.pr_key) loginInfo
							where pr_key in (select distinct PartnerId from _partners_exchangeHotelsList)";
			return (DbHelpers.ExecuteAndReturnTable(Query)
					.AsEnumerable()
					.Select(x => new Partner()
					{
						Id = x.Field<int>("Id"),
						Title = x.Field<string>("Title"),
						MarketId = x.Field<int>("MarketId"),
						Login = x.Field<string>("Login"),
						Password = x.Field<string>("Password")
					}).ToList());
		}
	}
	
	public class Hotel : DataItem
	{

	}

	public class CostofferList : DataItem
	{

	}

	public class DataItem
	{
		public int Id { get; set; }
		public string Title { get; set; }
	}

	public class ContractHeader
	{
		public string SupplierName { get; set; } = "Mouzenidis Travel Greece";
		public string Hotel { get; set; }
		public string HotelName { get; set; }
		public string Operator { get; set; } = "FIBULA ROM";
		public string OperatorName { get; set; } = "FIBULA ROMANIA";
		public DateTime BegDate { get; set; }
		public DateTime EndDate { get; set; }
		public string Currency { get; set; } = "EUR";
		public int PriceOpt { get; set; }
		public DateTime ResBegDate { get; set; }
		public DateTime ResEndDate { get; set; }
		public string PriceType { get; set; }
		public string Remark { get; set; }
	}

	public class ContractRoomData
	{
		public string Accom { get; set; }
		public string AccomName { get; set; }
		public string Room { get; set; }
		public string RoomName { get; set; }
		public string Board { get; set; }
		public string BoardName { get; set; }
		public DateTime BegDate { get; set; }
		public DateTime EndDate { get; set; }
		public decimal AdultPrice { get; set; }
		public decimal ExtBedPrice { get; set; }
		public decimal BoardPrice { get; set; }
		public string PriceOpt { get; set; }
		public string PriceType { get; set; }
		public int StdAdl { get; set; }
		public int MinAdl { get; set; }
		public int MaxAdl { get; set; }
		public int MaxChd { get; set; }
		public int MaxPax { get; set; }
	}

	public class ContractChildPriceData
	{
		public DateTime BegDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Adl { get; set; }
		public string Room { get; set; }
		public string Board { get; set; }
		public string Accom { get; set; }
		public int C1Age1 { get; set; }
		public int C1Age2 { get; set; }
		public int C1Per { get; set; }
		public decimal C1Price { get; set; }
		public int C2Age1 { get; set; }
		public int C2Age2 { get; set; }
		public int C2Per { get; set; }
		public decimal C2Price { get; set; }
		public int C3Age1 { get; set; }
		public int C3Age2 { get; set; }
		public int C3Per { get; set; }
		public decimal C3Price { get; set; }
		public int C4Age1 { get; set; }
		public int C4Age2 { get; set; }
		public int C4Per { get; set; }
		public decimal C4Price { get; set; }

	}

	public class OptionData
	{
		public string Room { get; set; }
		public DateTime BegDate { get; set; }
		public DateTime EndDate { get; set; }
		public int ReleaseDay { get; set; }
	}

	public class SPOData
	{
		public int SpoNo { get; set; }
		public string SpoType { get; set; }
		public DateTime SellBegDate { get; set; }
		public DateTime SellEndDate { get; set; }
		public string Room { get; set; }
		public string Board { get; set; }
		public DateTime ResBegDate { get; set; }
		public DateTime ResEndDate { get; set; }
		public int XStay { get; set; }
		public int YPay { get; set; }
		public decimal Price { get; set; }
		public decimal ExtBedPrice { get; set; }
		public decimal BoardPrice { get; set; }
		public int PriceOpt { get; set; }
		public string Remark { get; set; }
		public string PriceType { get; set; }
		public int DaySpoType { get; set; }
		public decimal EBSellPer { get; set; }
		public decimal EBSellPrice { get; set; }
		public string SpoOpt { get; set; }
		public string EBChdOpt { get; set; }
		public decimal SellPer { get; set; }
		public int FromDay { get; set; }
		public int ToDay { get; set; }
		public string SellCur { get; set; }
		public string ContSpo { get; set; }
		public string EarlyAppType { get; set; }
		public string LongStayType { get; set; }
		public int LongContDay { get; set; }
	}

	public class PricesData
	{
		public ContractHeader ContractHeader { get; set; }
		public List<ContractRoomData> contractRoomData { get; set; }
		public List<ContractChildPriceData> contractChildPriceContents { get; set; }
		public List<OptionData> options { get; set; }
		public List<SPOData> spos { get; set; } 
	}

	public class Partner : Item
	{
		public int MarketId { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
	}
}
