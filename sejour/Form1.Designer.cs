﻿namespace sejour
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbHotels = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCostOfferList = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgRoomsData = new System.Windows.Forms.DataGridView();
            this.dgRooms = new System.Windows.Forms.DataGridView();
            this.btnGetPricesData = new System.Windows.Forms.Button();
            this.btnSendContract = new System.Windows.Forms.Button();
            this.btnGetStopSalesData = new System.Windows.Forms.Button();
            this.btnSendStopSales = new System.Windows.Forms.Button();
            this.cbRegions = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMarkets = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSendRegionContracts = new System.Windows.Forms.Button();
            this.lStatus = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lToken = new System.Windows.Forms.Label();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.tbPass = new System.Windows.Forms.TextBox();
            this.lLogin = new System.Windows.Forms.Label();
            this.lPassword = new System.Windows.Forms.Label();
            this.btnGetPartnerHotelList = new System.Windows.Forms.Button();
            this.btnSendContracts = new System.Windows.Forms.Button();
            this.cbPriceOfferTypes = new System.Windows.Forms.ComboBox();
            this.cbPartners = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRoomsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRooms)).BeginInit();
            this.SuspendLayout();
            // 
            // cbHotels
            // 
            this.cbHotels.FormattingEnabled = true;
            this.cbHotels.Location = new System.Drawing.Point(61, 50);
            this.cbHotels.Name = "cbHotels";
            this.cbHotels.Size = new System.Drawing.Size(359, 21);
            this.cbHotels.TabIndex = 3;
            this.cbHotels.SelectedIndexChanged += new System.EventHandler(this.cbHotels_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hotel";
            // 
            // cbCostOfferList
            // 
            this.cbCostOfferList.FormattingEnabled = true;
            this.cbCostOfferList.Location = new System.Drawing.Point(665, 50);
            this.cbCostOfferList.Name = "cbCostOfferList";
            this.cbCostOfferList.Size = new System.Drawing.Size(425, 21);
            this.cbCostOfferList.TabIndex = 6;
            this.cbCostOfferList.SelectedIndexChanged += new System.EventHandler(this.cbCostOfferList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(626, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "SPO";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 149);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1351, 696);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgRoomsData);
            this.tabPage2.Controls.Add(this.dgRooms);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1343, 670);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgRoomsData
            // 
            this.dgRoomsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgRoomsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRoomsData.Location = new System.Drawing.Point(6, 293);
            this.dgRoomsData.Name = "dgRoomsData";
            this.dgRoomsData.Size = new System.Drawing.Size(1318, 356);
            this.dgRoomsData.TabIndex = 1;
            // 
            // dgRooms
            // 
            this.dgRooms.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRooms.Location = new System.Drawing.Point(6, 6);
            this.dgRooms.Name = "dgRooms";
            this.dgRooms.Size = new System.Drawing.Size(1318, 281);
            this.dgRooms.TabIndex = 0;
            // 
            // btnGetPricesData
            // 
            this.btnGetPricesData.Location = new System.Drawing.Point(1105, 15);
            this.btnGetPricesData.Name = "btnGetPricesData";
            this.btnGetPricesData.Size = new System.Drawing.Size(144, 32);
            this.btnGetPricesData.TabIndex = 16;
            this.btnGetPricesData.Text = "get contract data";
            this.btnGetPricesData.UseVisualStyleBackColor = true;
            this.btnGetPricesData.Visible = false;
            this.btnGetPricesData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnSendContract
            // 
            this.btnSendContract.Location = new System.Drawing.Point(1258, 15);
            this.btnSendContract.Name = "btnSendContract";
            this.btnSendContract.Size = new System.Drawing.Size(116, 32);
            this.btnSendContract.TabIndex = 17;
            this.btnSendContract.Text = "send contract";
            this.btnSendContract.UseVisualStyleBackColor = true;
            this.btnSendContract.Visible = false;
            this.btnSendContract.Click += new System.EventHandler(this.btnPrepareContract_Click);
            // 
            // btnGetStopSalesData
            // 
            this.btnGetStopSalesData.Location = new System.Drawing.Point(1105, 56);
            this.btnGetStopSalesData.Name = "btnGetStopSalesData";
            this.btnGetStopSalesData.Size = new System.Drawing.Size(144, 32);
            this.btnGetStopSalesData.TabIndex = 18;
            this.btnGetStopSalesData.Text = "get stop sales data";
            this.btnGetStopSalesData.UseVisualStyleBackColor = true;
            this.btnGetStopSalesData.Visible = false;
            this.btnGetStopSalesData.Click += new System.EventHandler(this.btnGetStopSalesData_Click);
            // 
            // btnSendStopSales
            // 
            this.btnSendStopSales.Location = new System.Drawing.Point(1258, 56);
            this.btnSendStopSales.Name = "btnSendStopSales";
            this.btnSendStopSales.Size = new System.Drawing.Size(116, 32);
            this.btnSendStopSales.TabIndex = 19;
            this.btnSendStopSales.Text = "send stop sales";
            this.btnSendStopSales.UseVisualStyleBackColor = true;
            this.btnSendStopSales.Visible = false;
            // 
            // cbRegions
            // 
            this.cbRegions.FormattingEnabled = true;
            this.cbRegions.Location = new System.Drawing.Point(905, 21);
            this.cbRegions.Name = "cbRegions";
            this.cbRegions.Size = new System.Drawing.Size(185, 21);
            this.cbRegions.TabIndex = 20;
            this.cbRegions.Visible = false;
            this.cbRegions.SelectedIndexChanged += new System.EventHandler(this.cbRegions_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(859, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Region";
            this.label3.Visible = false;
            // 
            // cbMarkets
            // 
            this.cbMarkets.FormattingEnabled = true;
            this.cbMarkets.Location = new System.Drawing.Point(665, 22);
            this.cbMarkets.Name = "cbMarkets";
            this.cbMarkets.Size = new System.Drawing.Size(164, 21);
            this.cbMarkets.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(621, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Market";
            // 
            // btnSendRegionContracts
            // 
            this.btnSendRegionContracts.Location = new System.Drawing.Point(1105, 94);
            this.btnSendRegionContracts.Name = "btnSendRegionContracts";
            this.btnSendRegionContracts.Size = new System.Drawing.Size(122, 32);
            this.btnSendRegionContracts.TabIndex = 23;
            this.btnSendRegionContracts.Text = "send region contracts";
            this.btnSendRegionContracts.UseVisualStyleBackColor = true;
            this.btnSendRegionContracts.Visible = false;
            this.btnSendRegionContracts.Click += new System.EventHandler(this.btnSendRegionContracts_Click);
            // 
            // lStatus
            // 
            this.lStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lStatus.Location = new System.Drawing.Point(472, 75);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(874, 37);
            this.lStatus.TabIndex = 24;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(59, 83);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(124, 29);
            this.btnConnect.TabIndex = 25;
            this.btnConnect.Text = "connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lToken
            // 
            this.lToken.Location = new System.Drawing.Point(56, 119);
            this.lToken.Name = "lToken";
            this.lToken.Size = new System.Drawing.Size(398, 17);
            this.lToken.TabIndex = 26;
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(293, 22);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(127, 20);
            this.tbLogin.TabIndex = 27;
            this.tbLogin.Text = "fibularo";
            // 
            // tbPass
            // 
            this.tbPass.Location = new System.Drawing.Point(470, 22);
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '*';
            this.tbPass.Size = new System.Drawing.Size(127, 20);
            this.tbPass.TabIndex = 28;
            this.tbPass.Text = "fat574968";
            // 
            // lLogin
            // 
            this.lLogin.AutoSize = true;
            this.lLogin.Location = new System.Drawing.Point(254, 25);
            this.lLogin.Name = "lLogin";
            this.lLogin.Size = new System.Drawing.Size(33, 13);
            this.lLogin.TabIndex = 29;
            this.lLogin.Text = "Login";
            // 
            // lPassword
            // 
            this.lPassword.AutoSize = true;
            this.lPassword.Location = new System.Drawing.Point(436, 25);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(30, 13);
            this.lPassword.TabIndex = 30;
            this.lPassword.Text = "Pass";
            // 
            // btnGetPartnerHotelList
            // 
            this.btnGetPartnerHotelList.Location = new System.Drawing.Point(185, 83);
            this.btnGetPartnerHotelList.Name = "btnGetPartnerHotelList";
            this.btnGetPartnerHotelList.Size = new System.Drawing.Size(127, 29);
            this.btnGetPartnerHotelList.TabIndex = 31;
            this.btnGetPartnerHotelList.Text = "get partner hotel list";
            this.btnGetPartnerHotelList.UseVisualStyleBackColor = true;
            this.btnGetPartnerHotelList.Click += new System.EventHandler(this.btnGetPartnerHotelList_Click);
            // 
            // btnSendContracts
            // 
            this.btnSendContracts.Location = new System.Drawing.Point(318, 83);
            this.btnSendContracts.Name = "btnSendContracts";
            this.btnSendContracts.Size = new System.Drawing.Size(139, 29);
            this.btnSendContracts.TabIndex = 32;
            this.btnSendContracts.Text = "send contracts";
            this.btnSendContracts.UseVisualStyleBackColor = true;
            this.btnSendContracts.Click += new System.EventHandler(this.btnSendContracts_Click);
            // 
            // cbPriceOfferTypes
            // 
            this.cbPriceOfferTypes.FormattingEnabled = true;
            this.cbPriceOfferTypes.Location = new System.Drawing.Point(470, 50);
            this.cbPriceOfferTypes.Name = "cbPriceOfferTypes";
            this.cbPriceOfferTypes.Size = new System.Drawing.Size(127, 21);
            this.cbPriceOfferTypes.TabIndex = 33;
            // 
            // cbPartners
            // 
            this.cbPartners.FormattingEnabled = true;
            this.cbPartners.Location = new System.Drawing.Point(61, 21);
            this.cbPartners.Name = "cbPartners";
            this.cbPartners.Size = new System.Drawing.Size(185, 21);
            this.cbPartners.TabIndex = 34;
            this.cbPartners.SelectionChangeCommitted += new System.EventHandler(this.cbPartners_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Partner";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(433, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 17);
            this.label6.TabIndex = 36;
            this.label6.Text = "Type";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 956);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbPartners);
            this.Controls.Add(this.cbPriceOfferTypes);
            this.Controls.Add(this.btnSendContracts);
            this.Controls.Add(this.btnGetPartnerHotelList);
            this.Controls.Add(this.lPassword);
            this.Controls.Add(this.lLogin);
            this.Controls.Add(this.tbPass);
            this.Controls.Add(this.tbLogin);
            this.Controls.Add(this.lToken);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.lStatus);
            this.Controls.Add(this.btnSendRegionContracts);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbMarkets);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbRegions);
            this.Controls.Add(this.btnSendStopSales);
            this.Controls.Add(this.btnGetStopSalesData);
            this.Controls.Add(this.btnSendContract);
            this.Controls.Add(this.btnGetPricesData);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbCostOfferList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbHotels);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgRoomsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRooms)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbHotels;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCostOfferList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgRooms;
        private System.Windows.Forms.Button btnGetPricesData;
        private System.Windows.Forms.DataGridView dgRoomsData;
        private System.Windows.Forms.Button btnSendContract;
        private System.Windows.Forms.Button btnGetStopSalesData;
        private System.Windows.Forms.Button btnSendStopSales;
        private System.Windows.Forms.ComboBox cbRegions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMarkets;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSendRegionContracts;
        private System.Windows.Forms.Label lStatus;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lToken;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.TextBox tbPass;
        private System.Windows.Forms.Label lLogin;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.Button btnGetPartnerHotelList;
        private System.Windows.Forms.Button btnSendContracts;
        private System.Windows.Forms.ComboBox cbPriceOfferTypes;
        private System.Windows.Forms.ComboBox cbPartners;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

